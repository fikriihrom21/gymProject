package com.example.aplikasiloginregister;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aplikasiloginregister.Adapter.AdpaterData;
import com.example.aplikasiloginregister.api.ApiClient;
import com.example.aplikasiloginregister.api.ApiInterface;
import com.example.aplikasiloginregister.model.Listgym.DataModel;
import com.example.aplikasiloginregister.model.Listgym.RetrieveListgym;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Listgym extends AppCompatActivity implements View.OnClickListener{
    private RecyclerView rvdata;
    private RecyclerView.Adapter addata;
    private RecyclerView.LayoutManager lmdata;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_gym);
        rvdata =findViewById(R.id.rvdata);
        lmdata = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rvdata.setLayoutManager(lmdata);
        Retrievedata();

    }

    public void Retrievedata(){
        ApiInterface arddata = ApiClient.getClient().create(ApiInterface.class);
        Call<RetrieveListgym> tampildata = arddata.ardRetrieveData();

        tampildata.enqueue(new Callback<RetrieveListgym>() {
            @Override
            public void onResponse(Call<RetrieveListgym> call, Response<RetrieveListgym> response) {
                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(Listgym.this, "kode: "+kode+" |Pesan :" +pesan, Toast.LENGTH_SHORT).show();
                listdata = response.body().getData();
                addata = new AdpaterData(Listgym.this,listdata);
                rvdata.setAdapter(addata);
                addata.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<RetrieveListgym> call, Throwable t) {
                Toast.makeText(Listgym.this,"Gagal menghubungi server",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private List<DataModel> listdata = new ArrayList<>();

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
