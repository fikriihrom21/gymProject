package com.example.aplikasiloginregister;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Menu extends AppCompatActivity implements View.OnClickListener {

    ImageButton MenuLatihan,MenuGYM,Menuberita,MenuDaftartempat,MenuManagemembership,MenuProfile;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        MenuLatihan = findViewById(R.id.MenuLatihan);
        MenuLatihan.setOnClickListener(this);
        MenuGYM = findViewById(R.id.MenuGYM);
        MenuGYM.setOnClickListener(this);
        Menuberita = findViewById(R.id.Menuberita);
        Menuberita.setOnClickListener(this);
        MenuDaftartempat = findViewById(R.id.MenuDaftartempat);
        MenuDaftartempat.setOnClickListener(this);
        MenuManagemembership = findViewById(R.id.MenuManagemembership);
        MenuManagemembership.setOnClickListener(this);
        MenuProfile = findViewById(R.id.MenuProfile);
        MenuProfile.setOnClickListener(this);
    }

    @Override
    public void onClick(View Menu) {
        switch (Menu.getId()){
                case R.id.MenuGYM:
                    Intent intent = new Intent(this,Listgym.class);
                    startActivity(intent);
                case R.id.MenuLatihan:
                    Intent intent1 = new Intent(this,Menulatihan.class);
                    startActivity(intent1);
                    //case R.id.Menuberita:
                    //setContentView(R.layout.berita);
                    //break;
                case R.id.MenuDaftartempat:
                    setContentView(R.layout.activity_tempatpendaftaran);
                    break;
                case R.id.MenuManagemembership:
                setContentView(R.layout.manage_membership);
                break;
            case R.id.MenuProfile:
               setContentView(R.layout.profile);
                break;


        }

    }
}
