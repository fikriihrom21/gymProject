package com.example.aplikasiloginregister;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Menulatihan extends AppCompatActivity implements View.OnClickListener{

 ImageButton LatihanGYMbtn,LatihanMandiribtn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LatihanGYMbtn = findViewById(R.id.LatihanGYMbtn);
        LatihanGYMbtn.setOnClickListener(this);
        LatihanMandiribtn = findViewById(R.id.LatihanMandiribtn);
        LatihanMandiribtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View MenuLatihan) {

        switch (MenuLatihan.getId()){
            case R.id.LatihanGYMbtn:
                setContentView(R.layout.menu_latihan_gym);
                break;
            case R.id.LatihanMandiribtn:
                setContentView(R.layout.menu_latihan_mandiri);
                break;

        }

    }

}
