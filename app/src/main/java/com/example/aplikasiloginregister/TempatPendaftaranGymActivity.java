package com.example.aplikasiloginregister;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.aplikasiloginregister.api.ApiClient;
import com.example.aplikasiloginregister.api.ApiInterface;
import com.example.aplikasiloginregister.model.tempat_gym.TempatGym;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TempatPendaftaranGymActivity extends AppCompatActivity implements View.OnClickListener{
    EditText etGymName,etTelpGym,etAddressGym,etPrice;
    Button btnRegisterGym,btnUploadNPWK,btnUploadTDP,btnUploadSIUP;
    String GymName,GymAddress,GymTelp,GymPrice;
    ApiInterface apiInterface;
    SessionManager sessionManager;
    ImageView imageView;
    int IMG_REQUEST = 21;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tempatpendaftaran);
        etGymName = findViewById(R.id.etRegisterGymName);
        etTelpGym = findViewById(R.id.etRegisterTelpGym);
        etAddressGym = findViewById(R.id.etRegisterAddressGym);
        etPrice = findViewById(R.id.etRegisterPrice);
        btnUploadNPWK = findViewById(R.id.btnUploadNPWK);
        btnUploadNPWK.setOnClickListener(this);
        btnUploadSIUP = findViewById(R.id.btnUploadSIUP);
        btnUploadSIUP.setOnClickListener(this);
        btnUploadTDP = findViewById(R.id.btnUploadTDP);
        btnUploadTDP.setOnClickListener(this);
        btnRegisterGym = findViewById(R.id.btnRegisterGym);
        btnRegisterGym.setOnClickListener(this);
        imageView = findViewById(R.id.imageView);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegisterGym:
                GymName = etGymName.getText().toString();
                GymAddress = etAddressGym.getText().toString();
                GymTelp = etTelpGym.getText().toString();
                GymPrice = etPrice.getText().toString();
                int value = 0;
                if(!"".equals(value)){
                    value=Integer.parseInt(GymPrice);
                }
                pendaftaranGym(GymName,GymAddress,GymAddress,GymPrice);
                break;
            case R.id.btnUploadSIUP:
                Intent intent= new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent,21);
                break;
        }
    }


    private void pendaftaranGym(String namaTempat, String alamatTempat, String noTelpTempat , String price){
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<TempatGym> call = apiInterface.tempatGymResponse(namaTempat,alamatTempat,noTelpTempat,price);
        call.enqueue(new Callback<TempatGym>() {
            @Override
            public void onResponse(Call<TempatGym> call, Response<TempatGym> response) {
                if (response.body() != null && response.isSuccessful() && response.body().isStatus()) {
                    Toast.makeText(TempatPendaftaranGymActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(TempatPendaftaranGymActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<TempatGym> call, Throwable t) {
                Toast.makeText(TempatPendaftaranGymActivity.this, t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }
        });


    }


}
