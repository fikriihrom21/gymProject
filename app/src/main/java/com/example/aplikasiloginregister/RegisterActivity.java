package com.example.aplikasiloginregister;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aplikasiloginregister.api.ApiClient;
import com.example.aplikasiloginregister.api.ApiInterface;
import com.example.aplikasiloginregister.model.register.Register;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etUsername,etPassword,etName,etEmail,etAddress,etPhone,etDate, etCPass;
    Button btnRegister;
    TextView tvLogin;
    String Username,Password,Name,Email,Address,Phone,Dob,Gender;
    ApiInterface apiInterface;
    Calendar myCalendar;
    int year,month,day;
    RadioGroup list_gender;
    RadioButton radioSexButton;
    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 4 characters
                    "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etUsername = findViewById(R.id.etRegisterUsername);
        etPassword = findViewById(R.id.etRegisterPassword);
        etName = findViewById(R.id.etRegisterName);
        etEmail = findViewById(R.id.etRegisterEmail);
        etAddress = findViewById(R.id.etRegisterAddressUser);
        etPhone = findViewById(R.id.etRegisterPhoneUser);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
        tvLogin = findViewById(R.id.tvLoginHere);
        tvLogin.setOnClickListener(this);
        list_gender = findViewById(R.id.radiogroupgender);
        etDate = findViewById(R.id.etRegisterDOB);
        etDate.setOnClickListener(this);
        etCPass = findViewById(R.id.etKonfirmasiPassword);

        myCalendar = Calendar.getInstance();
        year = myCalendar.get(Calendar.YEAR);
        month = myCalendar.get(Calendar.MONTH);
        day = myCalendar.get(Calendar.DAY_OF_MONTH);

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month= month+1;
                        String sdate = year+"-"+month+"-"+dayOfMonth;
                        etDate.setText(sdate);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegister:

                int checkedId = list_gender.getCheckedRadioButtonId();

                if (etUsername.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "username harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etName.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "name harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etEmail.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "email harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etPhone.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "telepon harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etAddress.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Address harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etDate.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "tanggal lahir harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etPassword.getText().toString().isEmpty()){
                    Toast.makeText(RegisterActivity.this, "password harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (checkedId < 1){
                    Toast.makeText(RegisterActivity.this, "gender harus dipilih !", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()){
                    Toast.makeText(RegisterActivity.this, "email harus sesuai dengan format !", Toast.LENGTH_SHORT).show();
                }else if (!PASSWORD_PATTERN.matcher(etPassword.getText().toString()).matches()){
                    Toast.makeText(RegisterActivity.this, "password harus sesuai format !", Toast.LENGTH_SHORT).show();
                }
                else if (!etCPass.getText().toString().matches(etPassword.getText().toString())){
                    Toast.makeText(RegisterActivity.this, "Konfirmasi password tidak sama dengan password yang dimasukkan !", Toast.LENGTH_SHORT).show();
                }
                else {
                    Username = etUsername.getText().toString();
                    Password = etPassword.getText().toString();
                    Name = etName.getText().toString();
                    Email = etEmail.getText().toString();
                    Address = etAddress.getText().toString();
                    Phone = etPhone.getText().toString();
                    Dob = etDate.getText().toString();
                    radioSexButton = (RadioButton) findViewById(checkedId);
                    Gender = radioSexButton.getText().toString();
                    register(Username,Password,Name,Email,Address,Phone,Dob,Gender);
                }
                break;
            case R.id.tvLoginHere:
                Intent intent  = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void register(String username, String password, String name, String email, String address, String phone, String dob, String gender) {
        
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Register> call = apiInterface.registerResponse(username, password, name, email, address, phone,gender,dob);
        call.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                if (response.body() != null && response.isSuccessful() && response.body().isStatus()) {
                    Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent  = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(RegisterActivity.this, response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }
        });



    }
}