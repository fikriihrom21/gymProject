package com.example.aplikasiloginregister;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Pembayaran_membership extends AppCompatActivity {
    //select item function
    String[] item = {"1 bulan","2 bulan","3 bulan","6 bulan","12 bulan"};
    AutoCompleteTextView selectplanmembership;
    ArrayAdapter <String> adapteritem;
    String[] pembayaran = {"1 Transfer Bank","2 Tunai"};
    AutoCompleteTextView selectpembayaran;
    ArrayAdapter <String> adapterpembayaran;

    //SELECT PLAN MEMBERSHIP
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pembayaran_membership);
        selectplanmembership = findViewById(R.id.selectplanmembership);
        adapteritem =  new ArrayAdapter<String>(this, R.layout.list_item,item);
        selectplanmembership.setAdapter(adapteritem);
        selectplanmembership.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                String item = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(),"item",Toast.LENGTH_SHORT).show();
            }
        }
            );

        //SELECT PEMBAYARAN
        selectpembayaran = findViewById(R.id.selectpembayaran);
        adapterpembayaran =  new ArrayAdapter<String>(this, R.layout.list_item,pembayaran);
        selectpembayaran.setAdapter(adapterpembayaran);
        selectplanmembership.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                String pembayaran = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(),"item",Toast.LENGTH_SHORT).show();
            }
        }
        );

    }
}
