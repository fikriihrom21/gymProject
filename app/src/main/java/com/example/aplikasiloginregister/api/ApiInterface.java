package com.example.aplikasiloginregister.api;

import com.example.aplikasiloginregister.model.Listgym.RetrieveListgym;
import com.example.aplikasiloginregister.model.login.Login;
import com.example.aplikasiloginregister.model.register.Register;
import com.example.aplikasiloginregister.model.tempat_gym.TempatGym;
import com.example.aplikasiloginregister.model.update_profile.UpdateProfile;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("login.php")
    Call<Login> loginResponse(
                @Field("username") String username,
                @Field("password") String password
    );

    @FormUrlEncoded
    @POST("register.php")
    Call<Register> registerResponse(
            @Field("username") String username,
            @Field("password") String password,
            @Field("name") String name,
            @Field("email") String email,
            @Field("alamat") String alamat,
            @Field("phone") String phone,
            @Field("jenis_kelamin") String jenis_kelamin,
            @Field("tanggal_lahir") String tanggal_lahir
    );

    @FormUrlEncoded
    @POST("Pendaftaran_Tempat.php")
    Call<TempatGym> tempatGymResponse(
            @Field("namaTempat") String namaTempat,
            @Field("alamatTempat") String alamatTempat,
            @Field("noTelpTempat") String noTelpTempat,
            @Field("price") String price
    );

    @FormUrlEncoded
    @POST("update_profile.php")
    Call<UpdateProfile> updateProfileResponse(
            @Field("username") String username,
            @Field("name") String name,
            @Field("alamat") String alamat,
            @Field("phone") String phone,
            @Field("jenis_kelamin") String jenis_kelamin,
            @Field("tanggal_lahir") String tanggal_lahir,
            @Field("id") String id

    );

    @FormUrlEncoded
    @GET("Retrievedatagym.php")
    Call<RetrieveListgym> ardRetrieveData();



}
