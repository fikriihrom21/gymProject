package com.example.aplikasiloginregister;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;

import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aplikasiloginregister.api.ApiClient;
import com.example.aplikasiloginregister.api.ApiInterface;
import com.example.aplikasiloginregister.model.login.LoginData;
import com.example.aplikasiloginregister.model.register.Register;
import com.example.aplikasiloginregister.model.update_profile.UpdateProfile;
import com.example.aplikasiloginregister.model.update_profile.UpdateProfileData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etUsername,etName,etAddress,etPhone,etDate;
    Button btnUpdate;
    TextView datedeparture;
    String Username,Name,Address,Phone,Dob,Gender, setUserName, setName, setAddress, setPhone, setId, setDOB,setGender;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    RadioGroup list_gender;
    RadioButton radioSexButton,mButton, fButton;
    ApiInterface apiInterface;
    SessionManager sessionManager;
    int year,month,day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        sessionManager = new SessionManager(UpdateProfileActivity.this);
        if (!sessionManager.isLoggedIn()){
            moveToLogin();
        }

        setUserName = sessionManager.getUserDetail().get(SessionManager.USERNAME);
        setName = sessionManager.getUserDetail().get(SessionManager.NAME);
        setAddress = sessionManager.getUserDetail().get(SessionManager.ALAMAT);
        setPhone = sessionManager.getUserDetail().get(SessionManager.PHONE);
        setDOB =sessionManager.getUserDetail().get(SessionManager.TANGGAL_LAHIR);
        setGender = sessionManager.getUserDetail().get(SessionManager.JENIS_KELAMIN);
        setId = sessionManager.getUserDetail().get(SessionManager.USER_ID);



        etUsername = findViewById(R.id.etEditUserName);
        etName = findViewById(R.id.etEditname);
        etAddress = findViewById(R.id.etEditAddress);
        etPhone = findViewById(R.id.etEditPhone);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        list_gender = findViewById(R.id.radiogroupgender2);
        etDate = findViewById(R.id.editDate);
        etDate.setOnClickListener(this);
        mButton = findViewById(R.id.rdlakilaki);
        fButton = findViewById(R.id.rdperempuan);


        //set isi profile
        etUsername.setText(setUserName);
        etName.setText(setName);
        etAddress.setText(setAddress);
        etPhone.setText(setPhone);
        etDate.setText(setDOB);

        if (setGender.equals("Laki-laki")){
            mButton.setChecked(true);
        }
        else if (setGender.equals("Perempuan")){
            fButton.setChecked(true);
        }

        myCalendar = Calendar.getInstance();
        year = myCalendar.get(Calendar.YEAR);
        month = myCalendar.get(Calendar.MONTH);
        day = myCalendar.get(Calendar.DAY_OF_MONTH);

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(UpdateProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month= month+1;
                        String sdate = year+"-"+month+"-"+dayOfMonth;
                        etDate.setText(sdate);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnUpdate:
                int checkedId = list_gender.getCheckedRadioButtonId();
                if (etUsername.getText().toString().isEmpty()){
                    Toast.makeText(UpdateProfileActivity.this, "username harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etName.getText().toString().isEmpty()){
                    Toast.makeText(UpdateProfileActivity.this, "name harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etPhone.getText().toString().isEmpty()){
                    Toast.makeText(UpdateProfileActivity.this, "telepon harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etAddress.getText().toString().isEmpty()){
                    Toast.makeText(UpdateProfileActivity.this, "Address harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (etDate.getText().toString().isEmpty()){
                    Toast.makeText(UpdateProfileActivity.this, "tanggal lahir harus diisi !", Toast.LENGTH_SHORT).show();
                }else if (checkedId < 1){
                    Toast.makeText(UpdateProfileActivity.this, "gender harus dipilih !", Toast.LENGTH_SHORT).show();
                }else
                {
                    Username = etUsername.getText().toString();
                    Name = etName.getText().toString();
                    Address = etAddress.getText().toString();
                    Phone = etPhone.getText().toString();
                    Dob = etDate.getText().toString();
                    radioSexButton = (RadioButton) findViewById(checkedId);
                    Gender = radioSexButton.getText().toString();
                    update(Username,Name,Address,Phone,Dob,Gender,setId);
                }
                break;
        }
    }

    private void update(String username, String name, String address, String phone, String dob, String gender, String id) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UpdateProfile> call = apiInterface.updateProfileResponse(username, name, address, phone,gender,dob,id);
       call.enqueue(new Callback<UpdateProfile>() {
           @Override
           public void onResponse(Call<UpdateProfile> call, Response<UpdateProfile> response) {
               if (response.body() != null && response.isSuccessful() && response.body().isStatus()) {

                   sessionManager = new SessionManager(UpdateProfileActivity.this);
                   UpdateProfileData updateProfileData = response.body().getUpdateProfileData();
                   sessionManager.createUpdateProfileSession(updateProfileData);


                   Toast.makeText(UpdateProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                   Intent intent  = new Intent(UpdateProfileActivity.this, MainActivity.class);
                   startActivity(intent);
                   finish();
               }else {
                   Toast.makeText(UpdateProfileActivity.this, response.body().getMessage(),Toast.LENGTH_SHORT).show();
               }
           }

           @Override
           public void onFailure(Call<UpdateProfile> call, Throwable t) {
               Toast.makeText(UpdateProfileActivity.this, t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
           }
       });
    }

    private void moveToLogin() {
        Intent intent = new Intent(UpdateProfileActivity.this,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }
}