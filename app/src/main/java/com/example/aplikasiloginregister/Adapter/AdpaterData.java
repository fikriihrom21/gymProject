package com.example.aplikasiloginregister.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aplikasiloginregister.R;
import com.example.aplikasiloginregister.model.Listgym.DataModel;

import java.util.List;

public class AdpaterData extends RecyclerView.Adapter<AdpaterData.HolderData> {

    private Context ctx;
    private List<DataModel>ListGYM;

    public AdpaterData(Context ctx,List<DataModel> ListGYM){
        this.ctx = ctx;
        this.ListGYM = ListGYM;

    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_item,parent,false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        DataModel dm = ListGYM.get(position);
        holder.namagym.setText(dm.getNamagym());
        holder.telpgym.setText(dm.getTelpgym());
        holder.hargamember.setText(String.valueOf(dm.getNamagym()));
        holder.alamat.setText(dm.getAlamat());
        holder.fasilitas.setText(dm.getFasilitas());
        holder.gambargym.setText(dm.getGanbargym());

    }

    @Override
    public int getItemCount() {
        return ListGYM.size();
    }

    public static class HolderData extends RecyclerView.ViewHolder {

        TextView namagym, telpgym, hargamember, fasilitas, alamat, gambargym;


        public HolderData(@NonNull View itemView) {
            super(itemView);
            namagym = itemView.findViewById(R.id.namagym);
            telpgym = itemView.findViewById(R.id.telpgym);
            hargamember = itemView.findViewById(R.id.hargamember);
            fasilitas = itemView.findViewById(R.id.fasilitas);
            alamat = itemView.findViewById(R.id.alamat);
            gambargym = itemView.findViewById(R.id.gambargym);


        }
    }
}
