package com.example.aplikasiloginregister;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.aplikasiloginregister.model.login.LoginData;
import com.example.aplikasiloginregister.model.update_profile.UpdateProfileData;

import java.util.HashMap;

public class SessionManager {

    private Context _context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public static final String IS_LOGGED_IN = "isLoggedIn";
    public static final String USER_ID = "user_id";
    public static final String USERNAME = "username";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String ALAMAT = "alamat";
    public static final String PHONE = "phone";
    public static final String JENIS_KELAMIN = "jenis_kelamin";
    public static final String TANGGAL_LAHIR = "tanggal_lahir";


    public SessionManager(Context context){
        this._context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public void createLoginSession(LoginData user){
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(USER_ID, user.getUserId());
        editor.putString(USERNAME, user.getUsername());
        editor.putString(NAME, user.getName());
        editor.putString(EMAIL, user.getEmail());
        editor.putString(ALAMAT, user.getAlamat());
        editor.putString(PHONE, user.getPhone());
        editor.putString(JENIS_KELAMIN, user.getJenis_kelamin());
        editor.putString(TANGGAL_LAHIR, user.getTanggal_lahir());
        editor.commit();
    }

    public void createUpdateProfileSession(UpdateProfileData updateProfileData){
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(USERNAME, updateProfileData.getUsername());
        editor.putString(NAME, updateProfileData.getName());
        editor.putString(EMAIL, updateProfileData.getEmail());
        editor.putString(ALAMAT, updateProfileData.getAlamat());
        editor.putString(PHONE, updateProfileData.getPhone());
        editor.putString(JENIS_KELAMIN, updateProfileData.getJenisKelamin());
        editor.putString(TANGGAL_LAHIR, updateProfileData.getTanggalLahir());
        editor.commit();
    }

    public HashMap<String,String> getUserDetail(){
        HashMap<String,String> user = new HashMap<>();
        user.put(USER_ID,sharedPreferences.getString(USER_ID, null));
        user.put(USERNAME,sharedPreferences.getString(USERNAME,null));
        user.put(NAME,sharedPreferences.getString(NAME,null));
        user.put(EMAIL,sharedPreferences.getString(EMAIL,null));
        user.put(ALAMAT,sharedPreferences.getString(ALAMAT,null));
        user.put(PHONE,sharedPreferences.getString(PHONE,null));
        user.put(JENIS_KELAMIN,sharedPreferences.getString(JENIS_KELAMIN,null));
        user.put(TANGGAL_LAHIR,sharedPreferences.getString(TANGGAL_LAHIR,null));
        return user;
    }

    public void logoutSession(){
        editor.clear();
        editor.commit();
    }

    public boolean isLoggedIn(){
        return  sharedPreferences.getBoolean(IS_LOGGED_IN,false);
    }

}
