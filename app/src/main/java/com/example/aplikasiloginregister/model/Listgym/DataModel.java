package com.example.aplikasiloginregister.model.Listgym;

public class DataModel {
    private int id;
    private String namagym,telpgym,hargamember,fasilitas,alamat,ganbargym;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamagym() {
        return namagym;
    }

    public void setNamagym(String namagym) {
        this.namagym = namagym;
    }

    public String getTelpgym() {
        return telpgym;
    }

    public void setTelpgym(String telpgym) {
        this.telpgym = telpgym;
    }

    public String getHargamember() {
        return hargamember;
    }

    public void setHargamember(String hargamember) {
        this.hargamember = hargamember;
    }

    public String getFasilitas() {
        return fasilitas;
    }

    public void setFasilitas(String fasilitas) {
        this.fasilitas = fasilitas;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getGanbargym() {
        return ganbargym;
    }

    public void setGanbargym(String ganbargym) {
        this.ganbargym = ganbargym;
    }
}
