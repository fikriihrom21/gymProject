package com.example.aplikasiloginregister.model.tempat_gym;

import com.google.gson.annotations.SerializedName;

public class TempatGymData {

	@SerializedName("alamatTempat")
	private String alamatTempat;

	@SerializedName("noTelpTempat")
	private String noTelpTempat;

	@SerializedName("namaTempat")
	private String namaTempat;

	@SerializedName("biaya_perbulan")
	private String biayaPerBulan;

	@SerializedName("img_siup")
	private String imgSiup;

	@SerializedName("img_tdp")
	private String imgTdp;

	@SerializedName("img_npwk")
	private String imgNpwk;

	@SerializedName("statusTempat")
	private String statusTempat;

	public void setAlamatTempat(String alamatTempat){
		this.alamatTempat = alamatTempat;
	}

	public String getAlamatTempat(){
		return alamatTempat;
	}

	public void setNoTelpTempat(String noTelpTempat){
		this.noTelpTempat = noTelpTempat;
	}

	public String getNoTelpTempat(){
		return noTelpTempat;
	}

	public void setNamaTempat(String namaTempat){
		this.namaTempat = namaTempat;
	}

	public String getNamaTempat(){
		return namaTempat;
	}

	public void setStatusTempat(String statusTempat){
		this.statusTempat = statusTempat;
	}

	public String getStatusTempat(){
		return statusTempat;
	}
}