package com.example.aplikasiloginregister.model.tempat_gym;

import com.google.gson.annotations.SerializedName;

public class TempatGym{

	@SerializedName("data")
	private TempatGymData tempatGymData;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setTempatGymData(TempatGymData tempatGymData){
		this.tempatGymData = tempatGymData;
	}

	public TempatGymData getTempatGymData(){
		return tempatGymData;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}
}