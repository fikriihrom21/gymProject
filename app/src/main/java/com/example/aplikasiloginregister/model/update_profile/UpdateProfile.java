package com.example.aplikasiloginregister.model.update_profile;

import com.google.gson.annotations.SerializedName;

public class UpdateProfile {

	@SerializedName("data")
	private UpdateProfileData updateProfileData;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setData(UpdateProfileData updateProfileData){
		this.updateProfileData = updateProfileData;
	}

	public UpdateProfileData getUpdateProfileData() {
		return updateProfileData;
	}

	public void setUpdateProfileData(UpdateProfileData updateProfileData) {
		this.updateProfileData = updateProfileData;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}
}